/*
Copyright 2015 SPECS Project - CeRICT

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

@author  Massimiliano Rak massimilinao.rak@unina2.it
@author  Valentina Casola casolav@unina.it
*/

package eu.specsproject.slaplatform.slamanager.api.restfrontend;

import java.io.IOException;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import eu.specsproject.slaplatform.slamanager.api.restbackend.SLAAPI;


public class SLAResource {

    private String slaId ;

    public SLAResource (String slaId){
        this.slaId= slaId;
    }
    
    @GET
    @Produces(MediaType.TEXT_XML)
    public String getSLA(){
        return SLAAPI.getInstance().getManager().retrieve(slaId).getSlaXML();
    }
    
    @PUT
    @Consumes(MediaType.TEXT_XML)
    public String updateSLA(String newSla)  throws ParserConfigurationException, SAXException, IOException{

        String slaXML= null;
        
        if (newSla!=null){
            SLAAPI.getInstance().getManager().update(slaId, newSla, null);    
        }
        return slaId;
    }
    
    @DELETE
    public String deleteSLA(){
        SLAAPI.getInstance().getManager().remove(slaId);    
        return slaId;
    }
    
    @Path("/lock")
    @GET
    public String getAndLockSLA(){
        return SLAAPI.getInstance().getManager().retrieveAndLock(slaId).toString();
    }
    
    
    @Path("/annotations")
    public SLAAnnotationsResource getAnnotationResource(){
        return new SLAAnnotationsResource(slaId);
    }
    
    @Path("/status")
    @GET
    public String getSLAstatus(){

        return SLAAPI.getInstance().getManager().getState(slaId).toString();
    }
    
    @Path("/customer")
    @GET
    public String getSLAcustomer(){

        return SLAAPI.getInstance().getManager().retrieveCustomerFromSla(slaId).toString();
    }

    @Path("/sign")
    @POST
    @Consumes(MediaType.TEXT_XML)
    public void signSLA(String newSla)  throws ParserConfigurationException, SAXException, IOException{
        
        String xml= null;
        
        if (newSla!=null && (!"".equalsIgnoreCase(newSla))){
            xml = newSla;
        }
        
        SLAAPI.getInstance().getManager().sign(slaId, xml, null);      
    }
    
    @Path("/userSign")
    @POST
    @Consumes(MediaType.TEXT_XML)
    public void userSignSLA(String newSla)  throws ParserConfigurationException, SAXException, IOException{
        
        String xml= null;
        
        if (newSla!=null && (!"".equalsIgnoreCase(newSla))){
            xml = newSla;
        }
        
        SLAAPI.getInstance().getManager().userSign(slaId, xml, null);      
    }
    
    @Path("/implement")
    @POST
    @Consumes(MediaType.TEXT_XML)
    public void implementSLA(String newSla){
        SLAAPI.getInstance().getManager().implement(slaId);      
    }

    @Path("/observe")
    @POST
    public void observeSLA(){
        SLAAPI.getInstance().getManager().observe(slaId);      
    }
    
    @Path("/complete")
    @POST
    public void completeSLA(){
        SLAAPI.getInstance().getManager().complete(slaId);     
    }

    @Path("/terminate")
    @POST
    public void terminateSLA(){
        SLAAPI.getInstance().getManager().terminate(slaId);        
    }


    @Path("/signalAlert")
    @POST
    public void signalAlert(){
        SLAAPI.getInstance().getManager().signalAlert(slaId);        
    }
    
    @Path("/signalViolation")
    @POST
    public void signalViolation(){
        SLAAPI.getInstance().getManager().signalViolation(slaId);        
    }

    @Path("/reNegotiate")
    @POST
    public void reNegotiate(){
        SLAAPI.getInstance().getManager().reNegotiate(slaId);      
    }
    
    @Path("/remediate")
    @POST
    public void remediate(){
        SLAAPI.getInstance().getManager().remediate(slaId);      
    }
    
    @Path("/redress")
    @POST
    public void redress(){
        SLAAPI.getInstance().getManager().redress(slaId);      
    }
    
    @Path("/unlock")
    @POST 
    public void unlockSLA(){
        SLAAPI.getInstance().getManager().unlock(slaId,null);      
    }

}
