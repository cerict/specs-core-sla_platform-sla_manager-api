/*
Copyright 2015 SPECS Project - CeRICT

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

@author  Massimiliano Rak massimilinao.rak@unina2.it
@author  Valentina Casola casolav@unina.it
*/

package eu.specsproject.slaplatform.slamanager.api.restfrontend;


import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.google.gson.Gson;

import eu.specsproject.slaplatform.slamanager.api.restbackend.SLAAPI;
import eu.specsproject.slaplatform.slamanager.entities.Annotation;

public class SLAAnnotationsResource {
    
    private String slaId ;

    public SLAAnnotationsResource (String slaId){
        this.slaId=slaId;
    }

    @GET
    public Response getAnnotations(){
        
    	String annotationsJSON = new Gson().toJson(SLAAPI.getInstance().getManager().getAnnotations(slaId));

		return Response.ok(annotationsJSON, MediaType.APPLICATION_JSON).build();       
    }
    
    
    @POST
    public void createAnnotation(@FormParam("key") String annotationKey,
            @FormParam("value") String annotationValue, @FormParam("timestamp") String timestamp){
        
    	if(annotationKey == null || annotationValue == null){
    			throw new IllegalArgumentException("The annotationKey and annotationValue params can not be null");
    	}
        Annotation ann = new Annotation(annotationKey, annotationValue, timestamp);
        
        SLAAPI.getInstance().getManager().annotate(slaId, ann);
    }
    
    
}
